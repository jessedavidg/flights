# Holiday Flight Performance

U.S. flight data was gathered and analyzed to assess the data-driven reality of Thanksgiving travel. 

The results can be viewed in this [report](https://bitbucket.org/jessedavidg/flights/raw/6cf60dd37e4fc674e0d54fa73f6bb7e1f891c6c4/Jesse_Green_SeedScientific_Assignment.pdf) and [webapp](http://thanksgiving-flights.appspot.com).